beast-config
============

This repository is used to store all BEAST servers configuration.

Each xml configuration shall be saved under a directory named after the server
fully-qualified domain name.
The name of the xml file shall be the name of the BEAST root component.

If there is no directory named after the server, the configuration file from the *default*
directory is used.

The script *validate_xml* is used to validate all xml files against the Alarm Configuration Schema.
It is run automatically by the gitlab-ci pipeline.
You can run it locally on your machine (OSX and Linux) if you have xmllint installed.

When pushing to master, the new configuration is automatically deployed to the BEAST servers on the TN.

See http://cs-studio.sourceforge.net/docbook/ch14.html#idp971040 for more information about BEAST configuration.
